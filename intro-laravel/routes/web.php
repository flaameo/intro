<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/luas', function () {
    return view('welcome');
});

route::get('/index', 'HomeController@index');
route::get('/form', 'AuthController@form');
route::post('/selamat', 'AuthController@selamat');