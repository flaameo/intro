<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('form');
    }

    public function selamat(Request $request)
    {
        $first = $request->first_name;
        $last = $request->last_name;

        return view('selamat', compact('first', 'last'));
    }
}
