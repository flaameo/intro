<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/selamat" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="GNDR"> Male<br>
        <input type="radio" name="GNDR"> Female<br>
        <input type="radio" name="GNDR"> Other<br><br>
        <label>Nasionality:</label><br><br>
        <select name="NISN"><br>
            <option value="indonesia">Indonesian</option>
            <option value="malaysia">Malaysia</option>
            <option value="belanda">Netherlands</option>
            <option value="arabsaudi">Saudi Arabia</option>
            <option value="amerikaserikat">United State of America</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>